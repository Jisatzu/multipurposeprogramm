﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.BookModel
{
    public interface IBookSeriesRepository
    {
        BookSerie GetBookSerie(Guid id);
        IEnumerable<BookSerie> GetAllBookSeries();
        void CreateBookSeries(BookSerie bookSerie);
    }
}
