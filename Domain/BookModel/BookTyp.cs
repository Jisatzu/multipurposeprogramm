﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.BookModel
{
    public class BookTyp : ValueObject
    {
        public Guid Id { get; private set; }
        public string Typ { get; private set; }

        public BookTyp(Guid id, string typ)
        {
            Id = id;
            Typ = typ;
        }
        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return Id;
            yield return Typ;
        }
    }
}
