﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.BookModel
{
    public class BookSerie : ValueObject
    {
        public Guid Id { get; private set; }
        public string Titel{ get; private set; }
        public int AmountOwned { get; private set; }
        public Book Book { get; private set; }
        public BookTyp BookTyp { get; private set; }
        public Genre FirstGenre { get; private set; }
        public Genre SecondGenre { get; private set; }
        public Genre ThirdGenre { get; private set; }
        public BookSerie(Guid id, string titel, int amountOwned, Book book, BookTyp bookTyp, Genre firstGenre, Genre secondGenre)
        {
            Id = id;
            Titel = titel;
            Book = book;
            BookTyp = bookTyp;
            FirstGenre = firstGenre;
            SecondGenre = secondGenre;
            ThirdGenre = null;
            AmountOwned = amountOwned;
        }
        public BookSerie(Guid id, string titel, int amountOwned, Book book, BookTyp bookTyp, Genre firstGenre, Genre secondGenre, Genre thirdGenre)
        {
            Id = id;
            Titel = titel;
            Book = book;
            BookTyp = bookTyp;
            FirstGenre = firstGenre;
            SecondGenre = secondGenre;
            ThirdGenre = thirdGenre;
            AmountOwned = amountOwned;
        }
        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return Id;
            yield return Titel;
            yield return AmountOwned;
            yield return Book;
            yield return BookTyp;
            yield return FirstGenre;
            yield return SecondGenre;
            yield return ThirdGenre;
        }
    }
}
