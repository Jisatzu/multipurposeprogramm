﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.BookModel
{
    public interface IBookTypRepository
    {
        BookTyp GetBookTyp(Guid id);
        IEnumerable<BookTyp> GetAllBookTyps();
        void CreateBookTyp(BookTyp bookTyp);
    }
}
