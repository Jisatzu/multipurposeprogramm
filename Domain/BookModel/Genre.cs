﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.BookModel
{
    public class Genre : ValueObject
    {
        public Guid Id { get; private set; }
        public string Name { get; private set; }

        public Genre(Guid id, string name)
        {
            Id = id;
            Name = name;
        }
        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return Id;
            yield return Name;
        }
    }
}
