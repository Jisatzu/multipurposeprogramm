﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.BookModel
{
    public interface IGenreRepository
    {
        Genre GetGenre(Guid id);
        IEnumerable<Genre> GetAllGenres();
        void CreateGenre(Genre genre);
    }
}
