﻿
using System;
using System.Collections.Generic;

namespace Domain.BookModel
{
    public interface IBookrepository
    {
        Book GetBook(Guid id);
        IEnumerable<Book> GetAllBooks();
        void CreateBook(Book book);
    }
}
