﻿using System;
using System.Collections.Generic;

namespace Domain.BookModel
{
    public class Book : ValueObject
    {
        public Guid Id { get; private set; }
        public string Titel { get; private set; }
        public int Volume { get; private set; }

        public Book(Guid id, string title, int volume)
        {
            Id = id;
            Titel = title;
            Volume = volume;
        }
        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return Id;
            yield return Titel;
            yield return Volume;
        }
    }
}
