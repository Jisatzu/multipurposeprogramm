﻿using Infrastructure.Book.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Book
{
    public static class BookMapper
    {
        public static BookSerieDTO ToDTO(this Domain.BookModel.BookSerie bookserie)
        {
            return new BookSerieDTO
            {
                Id = bookserie.Id,
                Titel = bookserie.Titel,
                AmountOwned = bookserie.AmountOwned,
                FirstGenreId = bookserie.FirstGenre.Id,
                SecondGenreId = bookserie.SecondGenre.Id,
                ThirdGenreId = bookserie.ThirdGenre.Id,
                BookTypId = bookserie.BookTyp.Id,
                BookId = bookserie.Book.Id
            };
        }
        public static BookDTO ToDTO(this Domain.BookModel.Book book)
        {
            return new BookDTO
            {
                Id = book.Id,
                Titel = book.Titel,
                Volume = book.Volume
            };
        }
        public static BookTypDTO ToDTO(this Domain.BookModel.BookTyp bookTyp)
        {
            return new BookTypDTO
            {
                Id = bookTyp.Id,
                Name = bookTyp.Typ
            };
        }
        public static GenreDTO ToDTO(this Domain.BookModel.Genre genre)
        {
            return new GenreDTO
            {
                Id = genre.Id,
                Name = genre.Name
            };
        }

        /*
        *  #################################
        *  Below here are all ToValueObjects
        *  #################################
        */

        public static Domain.BookModel.BookSerie ToValueObject(this BookSerieDTO bookSerieDTO,
            Domain.BookModel.Genre firstGenre,
            Domain.BookModel.Genre secondGenre,
            Domain.BookModel.Genre thirdGenre,
            Domain.BookModel.Book book,
            Domain.BookModel.BookTyp booktyp)
        {
            return new Domain.BookModel.BookSerie
            (
                bookSerieDTO.Id,
                bookSerieDTO.Titel,
                bookSerieDTO.AmountOwned,
                book,
                booktyp,
                firstGenre,
                secondGenre,
                thirdGenre
            );
        }
        public static Domain.BookModel.BookSerie ToValueObject(this BookSerieDTO bookSerieDTO,
            Domain.BookModel.Genre firstGenre,
            Domain.BookModel.Genre secondGenre,
            Domain.BookModel.Book book,
            Domain.BookModel.BookTyp booktyp)
        {
            return new Domain.BookModel.BookSerie
            (
                bookSerieDTO.Id,
                bookSerieDTO.Titel,
                bookSerieDTO.AmountOwned,
                book,
                booktyp,
                firstGenre,
                secondGenre
            );
        }
        public static Domain.BookModel.Book ToValueObject(this BookDTO book)
        {
            return new Domain.BookModel.Book(book.Id, book.Titel, book.Volume);
        }
        public static Domain.BookModel.BookTyp ToValueObject(this BookTypDTO bookTypDTO)
        {
            return new Domain.BookModel.BookTyp(bookTypDTO.Id,bookTypDTO.Name);
        }
        public static Domain.BookModel.Genre ToValueObject(this GenreDTO genreDTO)
        {
            return new Domain.BookModel.Genre(genreDTO.Id,genreDTO.Name);
        }
    }
}
