﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Book.DTOs
{
    public class BookDTO
    {
        public Guid Id { get; set; }
        public string Titel { get; set; }
        public int Volume { get; set; }
    }
}
