﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.BookModel;

namespace Infrastructure.Book
{
    public class InMemoryBookRepository : IBookrepository
    {
        private static readonly List<Domain.BookModel.Book> BookDTOs = new List<Domain.BookModel.Book>();
        public void CreateBook(Domain.BookModel.Book book)
        {
            BookDTOs.Add(book);
        }
        public Domain.BookModel.Book GetBook(Guid id)
        {
            return BookDTOs.Single(b => b.Id == id);
        }
        public IEnumerable<Domain.BookModel.Book> GetAllBooks()
        {
            return BookDTOs;
        }
    }
}
