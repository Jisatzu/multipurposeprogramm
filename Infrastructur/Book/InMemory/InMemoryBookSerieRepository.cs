﻿using Domain.BookModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Book
{
    public class InMemoryBookSerieRepository : IBookSeriesRepository
    {
        private static readonly List<BookSerie> BookSerieDTOs = new List<BookSerie>();
        public void CreateBookSeries(BookSerie bookSerie)
        {
            BookSerieDTOs.Add(bookSerie);
        }
        public BookSerie GetBookSerie(Guid id)
        {
            return BookSerieDTOs.Single(b => b.Id == id);
        }
        public IEnumerable<BookSerie> GetAllBookSeries()
        {
            return BookSerieDTOs;
        }
    }
}
