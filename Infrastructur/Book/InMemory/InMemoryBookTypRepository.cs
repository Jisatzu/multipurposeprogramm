﻿using Domain.BookModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Book
{
    public class InMemoryBookTypRepository : IBookTypRepository
    {
        private static readonly List<BookTyp> BookTypDTOs = new List<BookTyp>();
        public void CreateBookTyp(BookTyp bookTyp)
        {
            BookTypDTOs.Add(bookTyp);
        }

        public IEnumerable<BookTyp> GetAllBookTyps()
        {
            return BookTypDTOs;
        }

        public BookTyp GetBookTyp(Guid id)
        {
            return BookTypDTOs.Single(b => b.Id == id);
        }
    }
}
