﻿using Domain.BookModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Book
{
    public class InMemoryGenreRepository : IGenreRepository
    {
        private static readonly List<Genre> GenreDTOs = new List<Genre>();
        public void CreateGenre(Genre genre)
        {
            GenreDTOs.Add(genre);
        }
        public Genre GetGenre(Guid id)
        {
            return GenreDTOs.Single(b => b.Id == id);
        }
        public IEnumerable<Genre> GetAllGenres()
        {
            return GenreDTOs;
        }
    }
}
