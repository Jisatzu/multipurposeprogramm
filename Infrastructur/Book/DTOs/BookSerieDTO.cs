﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Book.DTOs
{
    public class BookSerieDTO
    {
        public Guid Id { get; set; }
        public string Titel { get; set; }
        public int AmountOwned { get; set; }
        public Guid BookId { get; set; }
        public Guid BookTypId { get; set; }
        public Guid FirstGenreId { get; set; }
        public Guid SecondGenreId { get; set; }
        public Guid ThirdGenreId { get; set; }
    }
}
