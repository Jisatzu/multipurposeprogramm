﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Book.DTOs
{
    public class BookTypDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
