﻿using System;
using Domain.BookModel;

namespace InfrastructureTest
{
    public static class TestFixtures
    {
        public static Book SampleBook => new Book(Guid.NewGuid(), "Sample", 3);
        public static Genre SampleGenre=> new Genre(Guid.NewGuid(), "Sample");
        public static Genre SampleGenre2=> new Genre(Guid.NewGuid(), "Sample2");
        public static Genre SampleGenre3=> new Genre(Guid.NewGuid(), "Sample3");
        public static BookTyp SampleBookTyp => new BookTyp(Guid.NewGuid(), "Sample");
        public static BookSerie SampleBookSerie => new BookSerie(Guid.NewGuid(), "Sample",3,SampleBook,SampleBookTyp,SampleGenre,SampleGenre2);
        public static BookSerie SampleBookSerie2 => new BookSerie(Guid.NewGuid(), "Sample",3,SampleBook,SampleBookTyp,SampleGenre,SampleGenre2,SampleGenre3);
    }
}