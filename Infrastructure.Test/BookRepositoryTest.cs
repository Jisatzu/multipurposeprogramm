﻿using System;
using System.Linq;
using Domain.BookModel;
using Infrastructure.Book;
using Xunit;

namespace InfrastructureTest
{
    public class BookRepositoryTest
    {
        [Fact]
        public void TestBookGetAll()
        {
            // Arrange
            var repo = new InMemoryBookRepository();

            var book2Id = Guid.NewGuid();
            var book = TestFixtures.SampleBook;
            var book2 = new Book(book2Id, "Title2", 3);
            repo.CreateBook(book);
            repo.CreateBook(book2);

            // Act

            var resultAllBooks = repo.GetAllBooks();
            var resultGetById = repo.GetBook(book.Id);

            // Assert

            Assert.Contains(book, resultAllBooks);
            Assert.Contains(book2, resultAllBooks);
            Assert.True(resultAllBooks.Count() == 2);
            Assert.Equal(book, resultGetById);

        }
        [Fact]
        public void TestGetAllBookTyps()
        {
            var repo = new InMemoryBookTypRepository();

            var bookTyp2Id = Guid.NewGuid();
            var bookTyp = TestFixtures.SampleBookTyp;
            var bookTyp2 = new BookTyp(bookTyp2Id, "Title2");
            repo.CreateBookTyp(bookTyp);
            repo.CreateBookTyp(bookTyp2);

            // Act

            var resultAllBookTpys = repo.GetAllBookTyps();
            var resultGetById = repo.GetBookTyp(bookTyp.Id);

            // Assert

            Assert.Contains(bookTyp, resultAllBookTpys);
            Assert.Contains(bookTyp2, resultAllBookTpys);
            Assert.True(resultAllBookTpys.Count() == 2);
            Assert.Equal(bookTyp, resultGetById);
        }
        [Fact]
        public void TestGetAllGenres()
        {
            var repo = new InMemoryGenreRepository();

            var genre2Id = Guid.NewGuid();
            var genre = TestFixtures.SampleGenre;
            var genre2 = new Genre(genre2Id, "Title2");
            repo.CreateGenre(genre);
            repo.CreateGenre(genre2);

            // Act

            var resultAllBookTpys = repo.GetAllGenres();
            var resultGetById = repo.GetGenre(genre.Id);

            // Assert

            Assert.Contains(genre, resultAllBookTpys);
            Assert.Contains(genre2, resultAllBookTpys);
            Assert.True(resultAllBookTpys.Count() == 2);
            Assert.Equal(genre, resultGetById);
        }
        [Fact]
        public void TestGetAllBookSeries()
        {
            var repo = new InMemoryBookSerieRepository();

            var bookSerie2Id = Guid.NewGuid();
            var bookSeries = TestFixtures.SampleBookSerie2;
            var book = TestFixtures.SampleBook;
            var bookTyp = TestFixtures.SampleBookTyp;
            var genre = TestFixtures.SampleGenre;
            var genre2 = TestFixtures.SampleGenre2;
            var genre3 = TestFixtures.SampleGenre3;
            var bookSerie2 = new BookSerie(bookSerie2Id,"Titel2",3,book,bookTyp,genre,genre2,genre3);
            repo.CreateBookSeries(bookSeries);
            repo.CreateBookSeries(bookSerie2);

            // Act

            var resultAllBookSeries = repo.GetAllBookSeries();
            var resultGetById = repo.GetBookSerie(bookSeries.Id);

            // Assert

            Assert.Contains(bookSeries, resultAllBookSeries);
            Assert.Contains(bookSerie2, resultAllBookSeries);
            Assert.True(resultAllBookSeries.Count() == 2);
            Assert.Equal(bookSeries, resultGetById);
        }
    }
}
